﻿using System;
using System.Collections.Generic;
using Calificaciones.Entidades;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Calificaciones
{
    public class Repositorio<T> where T:BaseDTO
    {
        private MongoClient client;
        private IMongoDatabase db;
        public Repositorio()
        {
            client = new MongoClient(new 
                                     MongoUrl(@"mongodb://cobaeh_docente:1234docente@ds217452.mlab.com:17452/cobaeh"));
            db = client.GetDatabase("cobaeh");
        }
        private IMongoCollection<T> Coleccion(){
            return db.GetCollection<T>(typeof(T).Name);
        }
        //CRUD
        public List<T> Read => Coleccion().AsQueryable<T>().ToList();

        public T Create(T entidad){
            entidad.Id = ObjectId.GenerateNewId();
            entidad.FechaHora = DateTime.Now;
            try
            {
                Coleccion().InsertOne(entidad);
                return entidad;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public bool Delete(ObjectId id){
            try
            {
                return Coleccion().DeleteOne(e => e.Id == id).DeletedCount == 1;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public T Update(T entidad){
            try
            {
                return Coleccion().ReplaceOne(e => e.Id == entidad.Id, entidad).
                                  ModifiedCount == 1 ? entidad : null;
            }
            catch (Exception)
            {
                return null;
            }
        }


    }
}
