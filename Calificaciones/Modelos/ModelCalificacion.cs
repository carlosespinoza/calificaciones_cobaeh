﻿using System;
using Calificaciones.Entidades;

namespace Calificaciones.Modelos
{
    public class ModelCalificacion
    {
        public Alumno Alumno
        {
            get;
            set;
        }
        public float Valor
        {
            get;
            set;
        }
        public ModelCalificacion()
        {
            
        }
    }
}
