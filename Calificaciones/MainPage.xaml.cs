﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Calificaciones
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            btnGrupos.Clicked+=BtnGrupos_Clicked;
            btnAlumnos.Clicked+=BtnAlumnos_Clicked;
            btnMaterias.Clicked+=BtnMaterias_Clicked;
            btnCaptura.Clicked+=BtnCaptura_Clicked;
            btnEstadisticos.Clicked+=BtnEstadisticos_Clicked;
        }

        void BtnEstadisticos_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Vistas.Estadisticos());
        }


        void BtnCaptura_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Vistas.Captura());
        }


        void BtnAlumnos_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Vistas.Alumnos());
        }


        void BtnGrupos_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Vistas.Grupos());
        }

        void BtnMaterias_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Vistas.Materias());
        }

    }
}
