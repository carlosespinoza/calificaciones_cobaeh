﻿using System;
using MongoDB.Bson;

namespace Calificaciones.Entidades
{
    public class Alumno:BaseDTO
    {
        public string Matricula { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public ObjectId GrupoId { get; set; }
        public Alumno()
        {
            
        }
        public override string ToString()
        {
            return string.Format("[{0}]{1} {2}", Matricula, Nombre, Apellidos);
        }
    }
}
