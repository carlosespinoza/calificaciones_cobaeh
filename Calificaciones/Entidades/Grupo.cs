﻿using System;
namespace Calificaciones.Entidades
{
    public class Grupo:BaseDTO
    {
        public int Clave
        {
            get;
            set;
        }
        public int Turno
        {
            get;
            set;
        }
        public int Semestre
        {
            get;
            set;
        }
        public string Numero
        {
            get;
            set;
        }
        public Grupo(int semestre, int turno, string numero)
        {
            Clave =int.Parse( string.Format("{0}{1}{2}", semestre, turno, numero));
            //Clave = int.Parse(semestre.ToString() + turno.ToString() + numero);
            Semestre = semestre;
            Numero = numero;
            Turno = turno;
        }
        public override string ToString()
        {
            return Clave.ToString();
        }
    }
}
