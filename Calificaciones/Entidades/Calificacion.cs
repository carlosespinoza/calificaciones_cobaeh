﻿using System;
using MongoDB.Bson;

namespace Calificaciones.Entidades
{
    public class Calificacion:BaseDTO
    {
        public ObjectId AlumnoId
        {
            get;
            set;
        }
        public ObjectId MateriaId
        {
            get;
            set;
        }
        public ObjectId GrupoId
        {
            get;
            set;
        }
        public float Valor
        {
            get;
            set;
        }
        public Calificacion()
        {
        }
    }
}
