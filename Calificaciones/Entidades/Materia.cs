﻿using System;
namespace Calificaciones.Entidades
{
    public class Materia:BaseDTO
    {
        public string Clave
        {
            get;
            set;
        }
        public string Nombre
        {
            get;
            set;
        }
        public Materia()
        {
        }
        public override string ToString()
        {
            return string.Format("[{0}] {1}", Clave, Nombre);
        }
    }
}
