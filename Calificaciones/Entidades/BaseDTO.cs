﻿using System;
using MongoDB.Bson;

namespace Calificaciones.Entidades
{
    public abstract class BaseDTO
    {
        public ObjectId Id { get; set; }
        public DateTime FechaHora { get; set; }
    }
}
