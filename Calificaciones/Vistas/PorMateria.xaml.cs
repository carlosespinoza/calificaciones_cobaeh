﻿using System;
using System.Collections.Generic;
using System.Linq;
using Calificaciones.Entidades;
using Calificaciones.Modelos;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using Xamarin.Forms;

namespace Calificaciones.Vistas
{
    public partial class PorMateria : ContentPage
    {
        Repositorio<Calificacion> calificacionesRepositorio;
        Repositorio<Materia> materiaRepositorio;
        Repositorio<Alumno> alumnoRepositorio;
        public PorMateria()
        {
            InitializeComponent();
            calificacionesRepositorio = new Repositorio<Calificacion>();
            materiaRepositorio = new Repositorio<Materia>();
            alumnoRepositorio = new Repositorio<Alumno>();

            pkrMaterias.ItemsSource = materiaRepositorio.Read;
            pkrMaterias.SelectedIndexChanged+=PkrMaterias_SelectedIndexChanged;

        }

        void PkrMaterias_SelectedIndexChanged(object sender, EventArgs e)
        {
            Materia materia = pkrMaterias.SelectedItem as Materia;

            List<Calificacion> calificaciones = calificacionesRepositorio.Read.Where
                      (h => h.MateriaId == materia.Id).ToList();

           
            List<Alumno> todosAlumnos = alumnoRepositorio.Read;

            List<Alumno> alumnosDeMateria = new List<Alumno>();

            foreach (var item in calificaciones)
            {
                alumnosDeMateria.Add(todosAlumnos.SingleOrDefault(a => a.Id == item.AlumnoId));
            }
            if (calificaciones.Count == 0)
            {
                DisplayAlert("Aviso", "No hay calificaciones capturadas para esta materia", "Ok");
            }
            else
            {
                lblCalificacion.Text = calificaciones.Max(j => j.Valor).ToString();

                Calificacion calMaxima = calificaciones.
                               FirstOrDefault(k => k.MateriaId ==
                                              materia.Id
                                && k.Valor == float.Parse(lblCalificacion.Text));

                lblNombre.Text = todosAlumnos.SingleOrDefault(k => k.Id
                              == calMaxima.AlumnoId).ToString();

                List<Alumno> alumnosReprobados = new List<Alumno>();
                foreach (var item in calificaciones.Where(j => j.Valor < 6.0))
                {
                    alumnosReprobados.Add(todosAlumnos.
                                Where(u => u.Id == item.AlumnoId).SingleOrDefault());
                }
                lstReprobados.ItemsSource = alumnosReprobados;


                List<modelGrafico> datosDeGrafico = new List<modelGrafico>();
                foreach (var item in calificaciones)
                {
                    datosDeGrafico.Add(new modelGrafico()
                    {
                        Calificacion = item.Valor,
                        Matricula = todosAlumnos.SingleOrDefault
                            (a => a.Id == item.AlumnoId).Matricula
                    });
                }

                PlotModel model = new PlotModel();
                CategoryAxis ejex = new CategoryAxis();
                ejex.Position = AxisPosition.Bottom;

                CategoryAxis ejey = new CategoryAxis();
                ejey.Position = AxisPosition.Left;

                model.Axes.Add(ejex);
                model.Axes.Add(ejey);

                model.Title = materia.Nombre;
                ColumnSeries columnas = new ColumnSeries();
                columnas.Title = "Calificaciones";
                foreach (var item in datosDeGrafico)
                {
                    columnas.Items.Add(new ColumnItem()
                    {
                        CategoryIndex = int.Parse(item.Matricula),
                        Value = item.Calificacion
                    });
                }
                model.Series.Add(columnas);

                chrGrafico.Model = model;

            }





            /*

            List<Calificacion> calificaciones = calificacionesRepositorio.Read.Where
                (j => j.MateriaId == (pkrMaterias.SelectedItem as Materia).Id).ToList();
            lblMateria.Text = "Datos de " + (pkrMaterias.SelectedItem as Materia).Nombre;
           

            lblCalificacion.Text= calificaciones.Max(j => j.Valor).ToString();


            Calificacion calMaxima = calificacionesRepositorio.Read.
                           FirstOrDefault(k => k.MateriaId == 
                                          (pkrMaterias.SelectedItem as Materia).Id 
                            && k.Valor == float.Parse(lblCalificacion.Text));


            lblNombre.Text = alumnoRepositorio.Read.SingleOrDefault(k => k.Id 
                          == calMaxima.AlumnoId).ToString();


            List<Calificacion> reprobados = calificacionesRepositorio.Read
                            .Where(k => k.Valor < 6.0 && k.MateriaId==
                                   (pkrMaterias.SelectedItem as Materia).Id).ToList();

            lstReprobados.ItemsSource = null;
            List<Alumno> alumnosReprobados = new List<Alumno>();
            foreach (var item in reprobados)
            {
                alumnosReprobados.Add(alumnoRepositorio.Read.
                            Where(u => u.Id == item.AlumnoId).SingleOrDefault());
            }
            lstReprobados.ItemsSource = alumnosReprobados;

*/
        }

    }
}
