﻿using System;
using System.Collections.Generic;
using Calificaciones.Entidades;
using Xamarin.Forms;

namespace Calificaciones.Vistas
{
    public partial class Materias : ContentPage
    {
        Repositorio<Materia> materiasRepositorio;
        bool editando;
        public Materias()
        {
            InitializeComponent();
            this.Title = "Materias";
            materiasRepositorio = new Repositorio<Materia>();
            ActualizarLista();
            btnGuardar.Clicked+=BtnGuardar_Clicked;
            btnEliminar.Clicked+=BtnEliminar_Clicked;
            lstMaterias.ItemSelected+=LstMaterias_ItemSelected;
        }

        void BtnGuardar_Clicked(object sender, EventArgs e)
        {
            if(editando){
                if(materiasRepositorio.Update(this.BindingContext as Materia)!=null){
                    DisplayAlert("Aviso", "Materia Actualizada", "Ok");
                    ActualizarLista();
                }else{
                    DisplayAlert("Error", "No se pudo actualizar la materia", "Ok");
                }
            }else{
                if(materiasRepositorio.Create(this.BindingContext as Materia)!=null){
                    DisplayAlert("Aviso", "Materia agregada", "Ok");
                    ActualizarLista();
                }
                else
                {
                    DisplayAlert("Error", "No se pudo agregar la materia", "Ok");
                }
            }
        }

        void BtnEliminar_Clicked(object sender, EventArgs e)
        {
            if(materiasRepositorio.Delete((this.BindingContext as Materia).Id)){
                DisplayAlert("Aviso", "Materia Eliminada", "Ok");
                ActualizarLista();
            }
            else
            {
                DisplayAlert("Error", "No se pudo eliminar la materia", "Ok");
            }
        }

        void LstMaterias_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            this.BindingContext = e.SelectedItem as Materia;
        }


        private void ActualizarLista()
        {
            lstMaterias.ItemsSource = null;
            try
            {
                lstMaterias.ItemsSource = materiasRepositorio.Read;
            }
            catch (Exception )
            {
                DisplayAlert("Error", "No puedo obtener los datos", "Ok");
            }
            this.BindingContext = new Materia();
            editando = false;
        }
    }
}
