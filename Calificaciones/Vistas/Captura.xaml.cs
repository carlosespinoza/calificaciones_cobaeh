﻿using System;
using System.Collections.Generic;
using System.Linq;
using Calificaciones.Entidades;
using Calificaciones.Modelos;
using Xamarin.Forms;

namespace Calificaciones.Vistas
{
    public partial class Captura : ContentPage
    {
        Repositorio<Grupo> grupoRepositorio;
        Repositorio<Alumno> alumnoRepositorio;
        Repositorio<Materia> materiaRepositorio;
        Repositorio<Calificacion> calificacionRepositorio;
        public Captura()
        {
            InitializeComponent();
            grupoRepositorio = new Repositorio<Grupo>();
            alumnoRepositorio = new Repositorio<Alumno>();
            materiaRepositorio = new Repositorio<Materia>();
            calificacionRepositorio = new Repositorio<Calificacion>();

            pkrGrupo.ItemsSource = grupoRepositorio.Read;
            pkrMateria.ItemsSource = materiaRepositorio.Read;

            pkrGrupo.SelectedIndexChanged+=PkrGrupo_SelectedIndexChanged;
            btnGuardar.Clicked+=BtnGuardar_Clicked;
        }

        void BtnGuardar_Clicked(object sender, EventArgs e)
        {
            
            foreach (ModelCalificacion item in lstCalificaciones.ItemsSource)
            {
                calificacionRepositorio.Create(new Calificacion()
                {
                    AlumnoId = item.Alumno.Id,
                    GrupoId = (pkrGrupo.SelectedItem as Grupo).Id,
                    MateriaId = (pkrMateria.SelectedItem as Materia).Id,
                    Valor = item.Valor
                });
            }
            DisplayAlert("Aviso", "Datos almacenados", "Ok");
            //foreach (var item in (lstCalificaciones.ItemsSource as List<ModelCalificacion>))
            //{
            //}
        }


        void PkrGrupo_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<ModelCalificacion> datos = new List<ModelCalificacion>();
            foreach (var item in alumnoRepositorio.Read.Where
                     (j=>j.GrupoId==(pkrGrupo.SelectedItem as Grupo).Id))
            {
                datos.Add(new ModelCalificacion()
                {
                    Alumno = item,
                    Valor = 0
                });
            }
            lstCalificaciones.ItemsSource = datos;
        }

    }
}
