﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Calificaciones.Vistas
{
    public partial class Estadisticos : ContentPage
    {
        public Estadisticos()
        {
            InitializeComponent();
            btnPorMateria.Clicked+=BtnPorMateria_Clicked;
        }

        void BtnPorMateria_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Vistas.PorMateria());
        }

    }
}
