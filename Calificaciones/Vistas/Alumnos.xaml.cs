﻿using System;
using System.Collections.Generic;
using Calificaciones.Entidades;
using Xamarin.Forms;

namespace Calificaciones.Vistas
{
    public partial class Alumnos : ContentPage
    {
        Repositorio<Alumno> alumnoRepositorio;
        Repositorio<Grupo> grupoRepositorio;
        bool editando;
        public Alumnos()
        {
            InitializeComponent();
            editando = false;
            alumnoRepositorio = new Repositorio<Alumno>();
            grupoRepositorio = new Repositorio<Grupo>();
            pkrMaterias.ItemsSource = grupoRepositorio.Read;
            this.BindingContext = new Alumno();
            ActualizarLista();
            btnGuardar.Clicked+=BtnGuardar_Clicked;
            btnEliminar.Clicked+=BtnEliminar_Clicked;
            lstAlumnos.ItemSelected+=LstAlumnos_ItemSelected;
        }

        void LstAlumnos_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            this.BindingContext = e.SelectedItem as Alumno;
        }


        void BtnGuardar_Clicked(object sender, EventArgs e)
        {
            (this.BindingContext as Alumno).GrupoId = (pkrMaterias.SelectedItem as Grupo).Id;
            if(editando){
                if (alumnoRepositorio.Update(this.BindingContext as Alumno) != null)
                {
                    DisplayAlert("Aviso", "Alumno Actualizado", "Ok");
                    ActualizarLista();
                    editando = false;
                }
                else
                {
                    DisplayAlert("Error", "No se pudo actualizar", "Ok");
                }
            }else{
                if(alumnoRepositorio.Create(this.BindingContext as Alumno)!=null){
                    DisplayAlert("Aviso", "Alumno Creado", "Ok");
                    ActualizarLista();
                }
                else
                {
                    DisplayAlert("Error", "No se pudo crear", "Ok");
                }
            }
        }

        void BtnEliminar_Clicked(object sender, EventArgs e)
        {
            if(alumnoRepositorio.Delete((this.BindingContext as Alumno).Id)){
                DisplayAlert("Aviso", "Alumno Eliminado","Ok");
                ActualizarLista();
            }else{
                DisplayAlert("Error", "No se pudo eliminar", "Ok");
            }
        }


        private void ActualizarLista()
        {
            lstAlumnos.ItemsSource = null;
            lstAlumnos.ItemsSource = alumnoRepositorio.Read;
            this.BindingContext = new Alumno();
        }
    }
}
