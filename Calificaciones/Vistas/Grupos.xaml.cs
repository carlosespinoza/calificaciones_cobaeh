﻿using System;
using System.Collections.Generic;
using Calificaciones.Entidades;
using Xamarin.Forms;

namespace Calificaciones.Vistas
{
    public partial class Grupos : ContentPage
    {

        Grupo grupoEditar;

        Repositorio<Grupo> grupoRepositorio;
        public Grupos()
        {
            InitializeComponent();
            grupoRepositorio = new Repositorio<Grupo>();
            ActualizarLista();
            grupoEditar = null;
            btnGuardar.Clicked += (object sender, EventArgs e) =>
            {

                if(grupoEditar==null){
                    //creando 
                    Grupo grupo = new Grupo(int.Parse(entSemestre.Text), int.Parse(entTurno.Text),
                    entNumero.Text);
                    if (grupoRepositorio.Create(grupo) != null)
                    {
                        DisplayAlert("Aviso", "Grupo creado", "Ok");
                        LimpiarCajas();
                        ActualizarLista();
                    }
                    else
                    {
                        DisplayAlert("Error", "No se creo el grupo", "Ok");
                    }
                }else{
                    //editando
                    Grupo grupo = new Grupo(int.Parse(entSemestre.Text), int.Parse(entTurno.Text),
                                          entNumero.Text);
                    grupo.Id = grupoEditar.Id;
                    grupo.FechaHora = grupoEditar.FechaHora;
                    if(grupoRepositorio.Update(grupo)!=null){
                        DisplayAlert("Aviso", "Grupo Actualizado", "Ok");
                        LimpiarCajas();
                        ActualizarLista();
                        grupoEditar = null;
                    }else{
                        DisplayAlert("Error", "No se pudo Actualizar", "Ok");
                    }
                }
            };  
            btnEliminar.Clicked+=BtnEliminar_Clicked;
            lstGrupos.ItemSelected+=LstGrupos_ItemSelected;
        }

        void LstGrupos_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            grupoEditar = lstGrupos.SelectedItem as Grupo;
            entTurno.Text = grupoEditar.Turno.ToString();
            entNumero.Text = grupoEditar.Numero;
            entSemestre.Text = grupoEditar.Semestre.ToString();

        }


        private void BtnEliminar_Clicked(object sender, EventArgs e)
        {
            Grupo grupo = lstGrupos.SelectedItem as Grupo;
            if(grupo!=null){
                if(grupoRepositorio.Delete(grupo.Id)){
                    DisplayAlert("Aviso", "Grupo Eleminado", "Ok");
                    ActualizarLista();
                    LimpiarCajas();
                }else{
                    DisplayAlert("Error", "No pude eliminar el grupo", "Ok");
                }
            }else{
                DisplayAlert("Aviso", "No has seleccionado grupo", "Ok");
            }
        }

       

       


        private void ActualizarLista()
        {
            try
            {
                lstGrupos.ItemsSource = null;
                lstGrupos.ItemsSource = grupoRepositorio.Read;
            }
            catch (Exception )
            {
                DisplayAlert("Error", "No pude obtener los datos", "Ok");
            }

        }

        private void LimpiarCajas()
        {
            entTurno.Text = "";
            entNumero.Text = "";
            entSemestre.Text = "";
        }
    }
}
